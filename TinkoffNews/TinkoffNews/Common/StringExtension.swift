//
//  StringExtension.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation

extension String {
    func stringByStrippingHTML() -> String {
        var stripedString: String = self.replacingOccurrences(of: "<[^>]+>", with: " ", options: String.CompareOptions.regularExpression, range: nil)
        stripedString = stripedString.replacingOccurrences(of: "&[^;]+;", with: " ", options: String.CompareOptions.regularExpression, range: nil)

        return stripedString
    }
}
