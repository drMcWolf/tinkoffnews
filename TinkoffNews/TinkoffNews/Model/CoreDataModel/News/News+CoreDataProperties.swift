//
//  News+CoreDataProperties.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 01.05.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: "News")
    }

    @NSManaged public var bankInfoTypeId: Int64
    @NSManaged public var identifier: String?
    @NSManaged public var name: String?
    @NSManaged public var pubDate: NSDate?
    @NSManaged public var text: String?
    @NSManaged public var toContent: Content?

}
