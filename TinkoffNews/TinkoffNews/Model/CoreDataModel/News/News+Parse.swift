//
//  News+Parse.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation

extension News {
    
    func fillNews(newsInfo: [String : Any]) {
        if let identifier = newsInfo["id"] as? String {
            self.identifier = identifier
        }
        
        if let pubDate = newsInfo["publicationDate"] as? [String : Any], let milliseconds = pubDate["milliseconds"] as? Double {
            let seconds = milliseconds/1000
            self.pubDate = NSDate(timeIntervalSince1970: seconds)
        }
        
        if let bankInfoTypeId = newsInfo["bankInfoTypeId"] as? Int64 {
            self.bankInfoTypeId = bankInfoTypeId
        }
        
        if let name = newsInfo["name"] as? String {
            self.name = name
        }
        
        if let text = newsInfo["text"] as? String {
            self.text = text.stringByStrippingHTML()
        }
    }
}
