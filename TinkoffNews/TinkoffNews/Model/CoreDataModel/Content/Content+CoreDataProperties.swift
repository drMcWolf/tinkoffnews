//
//  Content+CoreDataProperties.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 01.05.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation
import CoreData


extension Content {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Content> {
        return NSFetchRequest<Content>(entityName: "Content")
    }

    @NSManaged public var content: String?
    @NSManaged public var lastModificationDate: Double
    @NSManaged public var typeId: String?

}
