//
//  Content+Parse.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation

extension Content {
    func fillContent(contentInfo: [String : Any]) {
        
        if let lastModificationDate = contentInfo["lastModificationDate"] as? [String : Any], let milliseconds = lastModificationDate["milliseconds"] as? Double {
            self.lastModificationDate = milliseconds
        }
        
        if let content = contentInfo["content"] as? String {
            self.content = content.stringByStrippingHTML()
        }
        
        if let typeId = contentInfo["typeId"] as? String {
            self.typeId = typeId
        }
    }
}
