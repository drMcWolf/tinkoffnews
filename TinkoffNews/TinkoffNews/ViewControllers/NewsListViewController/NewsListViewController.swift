//
//  NewsListViewController.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import UIKit

private let kTableViewEstimateRowHeight: CGFloat = 44
private let kTableViewCellIdentifier = "NewsTableViewCell"
private let kShowNewsDetailSegueID = "showNewsDetailSegueID"
private let kErrorMessageTitle = "Ошибка"
private let kErrorActionTitle = "OK"

class NewsListViewController: UITableViewController, DataManagerDelegate {
    
    @IBOutlet weak var refreshConroll: UIRefreshControl!
    
    private var dataManager = DataManager()
    private var newsArray = [News]()
    
    private lazy var dateFormatterFullDate: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd:MM:YYYY HH:mm"
        return formatter
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.dataManager.delegate = self
        self.dataManager.updateNewsList()
    }
    
    private func configureTableView() {
        self.tableView.estimatedRowHeight = kTableViewEstimateRowHeight
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.register(UINib(nibName: kTableViewCellIdentifier, bundle: nil), forCellReuseIdentifier: kTableViewCellIdentifier)
    }
    
    private func updateTableView() {
        if let updatedNews = self.dataManager.getUpdatedObjects() {
            self.newsArray = updatedNews
            self.tableView.reloadData()
            if self.refreshConroll.isRefreshing {
                self.refreshConroll.endRefreshing()
            }
        }
    }
    
    private func showAlert(error: Error) {
        let alert = UIAlertController(title: kErrorMessageTitle, message: error.localizedDescription, preferredStyle: .alert)
        let action = UIAlertAction(title: kErrorActionTitle, style: .default, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
//MARK: - DataManagerDelegate

    func newsModelDidUpdate() {
        tableView.isUserInteractionEnabled = true
        self.updateTableView()
    }
    
    func payloadsContentDidUpdate() {
        tableView.isUserInteractionEnabled = true
        let selectedIndexPath = self.tableView.indexPathForSelectedRow
        let selectedNews = self.newsArray[selectedIndexPath!.row]
        let savedContent = self.dataManager.getUpdatedContent(id: selectedNews.identifier!)
        self.performSegue(withIdentifier: kShowNewsDetailSegueID, sender: savedContent)
    }
    
    func modelUpdateDidFailWithError(error: Error) {
        tableView.isUserInteractionEnabled = true
        self.updateTableView()
        self.showAlert(error: error)
    }

//MARK: - UITableViewDelegate, UITableViewDataSource
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NewsTableViewCell = tableView.dequeueReusableCell(withIdentifier: kTableViewCellIdentifier, for: indexPath) as! NewsTableViewCell
        let newsObject = self.newsArray[indexPath.row]
        cell.titleLabel.text = newsObject.text
        if let pubDate = newsObject.pubDate as Date? {
            cell.dateLabel.text = self.dateFormatterFullDate.string(from: pubDate)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedObject = self.newsArray[indexPath.row]
        tableView.isUserInteractionEnabled = false
        self.dataManager.getNewsPayloadsContent(id: selectedObject.identifier!)
    }

//MARK: - IBactions
    @IBAction func pullToRefresh(_ sender: UIRefreshControl) {
        tableView.isUserInteractionEnabled = false
        self.dataManager.updateNewsList()
    }
//MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == kShowNewsDetailSegueID {
            let destinationVC = segue.destination as? NewsDetailViewController
            destinationVC?.newsContent = sender as? Content
        }
    }
}

