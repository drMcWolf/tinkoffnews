//
//  NewsDetailViewController.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    var newsContent: Content?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.isScrollEnabled = false
        self.textView.text = newsContent?.content
        self.textView.isScrollEnabled = true
    }



}
