//
//  SessionManager.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation
import UIKit

private let kRequestTimeOut: TimeInterval = 10

class SessionManager {
    private var session: URLSession!
    
    init() {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = kRequestTimeOut
        self.session = URLSession(configuration: sessionConfig)
    }
    
    func GET(stringUrl: String, completion: @escaping Completion) {
        let url = URL(string: stringUrl)
        if let url = url {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            let dataTask = self.session.dataTask(with: url) { (data, response, error) in
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    completion(data, error)
                }
            }
            dataTask.resume()
        }
        
    }

}
