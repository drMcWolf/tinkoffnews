//
//  DataManager.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation

protocol DataManagerDelegate: class {
    
}

typealias Completion = ((Data?, Error?) -> ())

class DataManager {

    weak var delegate: DataManagerDelegate!
    private let apiManager = ApiManager()
    
    func updateNewsList() {
        self.apiManager.updateNewsList { (responce, error) in
            
        }
    }
    
    func getNewsPayloads(id: String) {
        self.apiManager.getNewsPayloads(id: id) { (responce, error) in
            
        }
    }
}
