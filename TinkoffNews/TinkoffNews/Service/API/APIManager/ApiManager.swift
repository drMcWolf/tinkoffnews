//
//  ApiManager.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation

private enum URLList: String {
    case NewsList = "https://api.tinkoff.ru/v1/news"
    case Payloads = "https://api.tinkoff.ru/v1/news_content?id="
}

class ApiManager {
    private var sessionManager = SessionManager()
    
    func updateNewsList(completion: @escaping Completion) {
        self.sessionManager.GET(stringUrl: URLList.NewsList.rawValue, completion: completion)
    }
    
    func getNewsPayloads(id: String, completion: @escaping Completion) {
        let stringUrl = URLList.Payloads.rawValue.appending(id)
        self.sessionManager.GET(stringUrl: stringUrl, completion: completion)
    }
}
