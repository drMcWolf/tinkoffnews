//
//  DataManager.swift
//  TinkoffNews
//
//  Created by Макаров Иван on 30.04.17.
//  Copyright © 2017 Макаров Иван. All rights reserved.
//

import Foundation
import CoreData

protocol DataManagerDelegate: class {
    func newsModelDidUpdate()
    func payloadsContentDidUpdate()
    func modelUpdateDidFailWithError(error: Error)
}

typealias Completion = ((Data?, Error?) -> ())

class DataManager {

    weak var delegate: DataManagerDelegate!
    private let apiManager = ApiManager()
    private let storageManager = StorageManager()
    
    func getUpdatedObjects() -> [News]? {
        let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "pubDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            let newsArray = try self.storageManager.defaultContext.fetch(fetchRequest)
            return newsArray
        }catch {
            return nil
        }
        
    }
    
    func getUpdatedContent(id:String) -> Content? {
        let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "identifier = %@", id)
        let newsArray = try? self.storageManager.defaultContext.fetch(fetchRequest)
        let savedContent = newsArray?.first?.toContent
        return savedContent
    }
    
    func updateNewsList() {
        self.apiManager.updateNewsList {[weak self] (response, error) in
            if error != nil {
                self?.delegate.modelUpdateDidFailWithError(error: error!)
                return
            }
            let responceDictionary = try? JSONSerialization.jsonObject(with: response!, options: []) as? [String : Any]
            if let dictionary = responceDictionary, let payload = dictionary?["payload"] as? [[String : Any]]  {
                let context = self?.storageManager.privateContext
                if let privateContext = context {
                    privateContext.perform {
                        for newsInfo in payload {
                            let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
                            fetchRequest.predicate = NSPredicate(format: "identifier = %@", newsInfo["id"] as! String)
                            
                            if let savedNews = try? privateContext.fetch(fetchRequest), savedNews.count != 0 {
                                savedNews.first?.fillNews(newsInfo: newsInfo)
                            }else {
                                let newNews = NSEntityDescription.insertNewObject(forEntityName: "News", into: privateContext) as? News
                                newNews?.fillNews(newsInfo: newsInfo)
                            }
                        }
                        self?.storageManager.savePrivateContext()
                        DispatchQueue.main.async {
                            self?.storageManager.saveContext()
                            self?.delegate.newsModelDidUpdate()
                        }
                    }
                }
            }
        }
    }
    
    func getNewsPayloadsContent(id: String) {
        self.apiManager.getNewsPayloads(id: id) {[weak self] (response, error) in
            if error != nil {
                if self?.getUpdatedContent(id: id) == nil {
                    self?.delegate.modelUpdateDidFailWithError(error: error!)
                }else {
                    self?.delegate.payloadsContentDidUpdate()
                }
                
                return
            }
            let responceDictionary = try? JSONSerialization.jsonObject(with: response!, options: []) as? NSDictionary
            if let dictionary = responceDictionary, let payload = dictionary?["payload"] as? [String : Any]  {

                let context = self?.storageManager.privateContext
                if let privateContext = context {
                    privateContext.perform {
                        let fetchRequest:NSFetchRequest<News> = News.fetchRequest()
                        fetchRequest.predicate = NSPredicate(format: "identifier = %@", id)
                        if let savedNews = try? privateContext.fetch(fetchRequest), savedNews.count != 0 {
                            let oldContent = savedNews.first?.toContent
                            if oldContent == nil || oldContent?.lastModificationDate != payload["lastModificationDate"] as? Double {
                                let newContent = NSEntityDescription.insertNewObject(forEntityName: "Content", into: privateContext) as? Content
                                newContent?.fillContent(contentInfo: payload)
                                
                                savedNews.first?.toContent = newContent
                            }
                            self?.storageManager.savePrivateContext()
                            DispatchQueue.main.async {
                                self?.storageManager.saveContext()
                                self?.delegate.payloadsContentDidUpdate()
                            }
                        }
                        
                    }
                }
            
            }
        }
    }
}
